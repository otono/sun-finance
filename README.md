# Test task. Small applications API service.

## Install
`make build`

## Load dummy data
`docker exec -it sf-php-fpm bash`

`php bin/console doctrine:fixtures:load -n`

## Testing
`docker exec -it sf-php-fpm bash`

`make tests`

## API documentation
http://localhost:8081/api/doc
