SHELL := /bin/bash

.PHONY: build
build:
	cd docker && docker-compose up --build

tests: export APP_ENV=test
tests:
	APP_ENV=test php bin/console doctrine:database:drop --force || true
	APP_ENV=test php bin/console doctrine:database:create
	APP_ENV=test php bin/console doctrine:migrations:migrate -n
	APP_ENV=test php bin/console doctrine:fixtures:load -n
	APP_ENV=test php ./vendor/bin/phpunit $@
.PHONY: tests