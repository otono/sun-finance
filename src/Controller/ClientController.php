<?php

namespace App\Controller;

use App\Entity\Client;
use App\Repository\ClientRepositoryInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\ClientService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

class ClientController extends AbstractApiController
{
    /**
     * Clients list
     *
     * @OA\Tag(name="Clients")
     * @OA\Response(
     *     response=200,
     *     description="Returns paginated clients list",
     *     @OA\JsonContent(type="object",
     *      @OA\Property(type="array", property="items",
     *          @OA\Items(ref=@Model(type=Client::class))
     *      ),
     *      @OA\Property(type="object", property="pagination",
     *          @OA\Property(type="number", property="page", default="1"),
     *          @OA\Property(type="number", property="pages_count", default="5"),
     *          @OA\Property(type="number", property="page_size", default="10"),
     *          @OA\Property(type="number", property="total", default="50")
     *      )
     *     )
     * )
     * @OA\Parameter(name="page", in="query", description="Page", @OA\Schema(type="string"))
     *
     * @param Request $request
     * @param ClientRepositoryInterface $clientRepository
     * @return Response
     */
    public function list(Request $request, ClientRepositoryInterface $clientRepository): Response
    {
        $page = $request->get('page') ?? 1;
        $paginator = new Paginator($clientRepository->getAllPaginatedQuery());
        return $this->jsonPaginated($paginator, $page);
    }

    /**
     * Client info
     *
     * @OA\Tag(name="Clients")
     * @OA\Response(
     *     response=200,
     *     description="Returns client info",
     *     @OA\JsonContent(type="object",
     *      @OA\Property(type="object", property="data", ref=@Model(type=Client::class))
     *     )
     * )
     *
     * @param int $id
     * @param ClientRepositoryInterface $clientRepository
     * @return Response
     */
    public function item(int $id, ClientRepositoryInterface $clientRepository): Response
    {
        $client = $clientRepository->find($id);

        if (!$client) {
            throw new NotFoundHttpException('Client not found.');
        }

        return $this->json($client, Response::HTTP_OK);
    }

    /**
     * Add new client
     *
     * @OA\Tag(name="Clients")
     * @OA\Response(
     *     response=201,
     *     description="Returns created client",
     *     @OA\JsonContent(type="object",
     *      @OA\Property(type="object", property="data", ref=@Model(type=Client::class))
     *     )
     * )
     * @OA\Parameter(name="firstName", in="query", required=true, @OA\Schema(type="string"))
     * @OA\Parameter(name="lastName", in="query", required=true, @OA\Schema(type="string"))
     * @OA\Parameter(name="email", in="query", required=true, @OA\Schema(type="string"))
     * @OA\Parameter(name="phoneNumber", in="query", required=true, @OA\Schema(type="string"))
     *
     * @param Request $request
     * @param ClientService $clientService
     * @param SerializerInterface $serializer
     * @return Response
     * @throws \App\Exception\ValidationException
     */
    public function add(Request $request, ClientService $clientService, SerializerInterface $serializer): Response
    {
        $client = $serializer->deserialize($request->getContent(), Client::class, 'json');
        $clientService->saveClient($client);

        return $this->json($client, Response::HTTP_CREATED);
    }

    /**
     * Update client
     *
     * @OA\Tag(name="Clients")
     * @OA\Response(
     *     response=200,
     *     description="Returns updated client",
     *     @OA\JsonContent(type="object",
     *      @OA\Property(type="object", property="data", ref=@Model(type=Client::class))
     *     )
     * )
     * @OA\Parameter(name="firstName", in="query", required=true, @OA\Schema(type="string"))
     * @OA\Parameter(name="lastName", in="query", required=true, @OA\Schema(type="string"))
     * @OA\Parameter(name="email", in="query", required=true, @OA\Schema(type="string"))
     * @OA\Parameter(name="phoneNumber", in="query", required=true, @OA\Schema(type="string"))
     *
     * @param Request $request
     * @param int $id
     * @param ClientRepositoryInterface $clientRepository
     * @param ClientService $clientService
     * @param SerializerInterface $serializer
     * @return Response
     * @throws \App\Exception\ValidationException
     */
    public function update(
        Request $request,
        int $id,
        ClientRepositoryInterface $clientRepository,
        ClientService $clientService,
        SerializerInterface $serializer
    ): Response
    {
        $client = $clientRepository->find($id);
        if (!$client) {
            throw new NotFoundHttpException('Client not found.');
        }

        $serialized = new Client();
        $serializer->deserialize($request->getContent(), Client::class, 'json',  [
            AbstractNormalizer::OBJECT_TO_POPULATE => $serialized,
            'groups' => 'update'
        ]);
        $clientService->updateClient($client, $serialized);

        return $this->json($client, Response::HTTP_OK);
    }

    /**
     * Delete client
     *
     * @OA\Tag(name="Clients")
     * @OA\Response(
     *     response=200,
     *     description="Returns delete status",
     *     @OA\JsonContent(type="object",
     *      @OA\Property(type="object", property="data",
     *          @OA\Property(type="string", property="message", default="Client deleted."),
     *      )
     *     )
     * )
     *
     * @param int $id
     * @param ClientRepositoryInterface $clientRepository
     * @param ClientService $clientService
     * @return Response
     */
    public function delete(int $id, ClientRepositoryInterface $clientRepository, ClientService $clientService): Response
    {
        $client = $clientRepository->find($id);
        if (!$client) {
            throw new NotFoundHttpException('Client not found.');
        }

        $clientService->deleteClient($client);

        return $this->json(['message' => 'Client deleted.'], Response::HTTP_OK);
    }
}
