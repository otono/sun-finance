<?php

declare(strict_types=1);

namespace App\Controller;

use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

abstract class AbstractApiController extends AbstractController
{
    protected ContainerBagInterface $params;

    /**
     * AbstractApiController constructor.
     * @param ContainerBagInterface $params
     */
    public function __construct(ContainerBagInterface $params)
    {
        $this->params = $params;
    }

    /**
     * @param $data
     * @param int $status
     * @param array $headers
     * @param array $context
     * @return JsonResponse
     */
    public function json($data, int $status = 200, array $headers = [], array $context = []): JsonResponse
    {
        return parent::json(['data' => $data], $status, $headers, $context);
    }

    /**
     * @param Paginator $paginator
     * @param int $page
     * @return JsonResponse
     */
    public function jsonPaginated(Paginator $paginator, int $page): JsonResponse
    {
        $totalItems = count($paginator);

        $pageSize = $this->params->get('app.pagination.page_size');
        $pagesCount = ceil($totalItems / $pageSize);

        // 404
        if ($page > $pagesCount) {
            throw new NotFoundHttpException('Page not found.');
        }

        $paginator
            ->getQuery()
            ->setFirstResult($pageSize * ($page-1))
            ->setMaxResults($pageSize);


        return $this->json([
            'items' => $paginator,
            'pagination' => [
                'page' => $page,
                'pages_count' => $pagesCount,
                'page_size' => $pageSize,
                'total' => $totalItems
            ]
        ], Response::HTTP_OK);
    }
}