<?php

namespace App\Controller;

use App\Entity\Application;
use App\Repository\ApplicationRepositoryInterface;
use App\Repository\ClientRepositoryInterface;
use App\Service\ApplicationService;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

class ApplicationController extends AbstractApiController
{
    protected SerializerInterface $serializer;

    /**
     * ApplicationController constructor.
     * @param ContainerBagInterface $params
     */
    public function __construct(ContainerBagInterface $params)
    {
        parent::__construct($params);
    }

    /**
     * Applications list
     *
     * @OA\Tag(name="Applications")
     * @OA\Response(
     *     response=200,
     *     description="Returns paginated applications list",
     *     @OA\JsonContent(type="object",
     *      @OA\Property(type="array", property="items",
     *          @OA\Items(ref=@Model(type=Application::class))
     *      ),
     *      @OA\Property(type="object", property="pagination",
     *          @OA\Property(type="number", property="page", default="1"),
     *          @OA\Property(type="number", property="pages_count", default="5"),
     *          @OA\Property(type="number", property="page_size", default="10"),
     *          @OA\Property(type="number", property="total", default="50")
     *      )
     *     )
     * )
     * @OA\Parameter(name="page", in="query", description="Page", @OA\Schema(type="string"))     *
     *
     * @param Request $request
     * @param ApplicationRepositoryInterface $applicationRepository
     * @return Response
     */
    public function list(Request $request, ApplicationRepositoryInterface $applicationRepository): Response
    {
        $page = $request->get('page') ?? 1;
        $paginator = new Paginator($applicationRepository->getAllPaginatedQuery());
        return $this->jsonPaginated($paginator, $page);
    }

    /**
     * Application info
     *
     * @OA\Tag(name="Applications")
     * @OA\Response(
     *     response=200,
     *     description="Returns application info",
     *     @OA\JsonContent(type="object",
     *      @OA\Property(type="object", property="data", ref=@Model(type=Application::class))
     *     )
     * )
     *
     * @param int $id
     * @param ApplicationRepositoryInterface $applicationRepository
     * @return Response
     */
    public function item(int $id, ApplicationRepositoryInterface $applicationRepository): Response
    {
        $application = $applicationRepository->find($id);

        if (!$application) {
            throw new NotFoundHttpException('Application not found.');
        }

        return $this->json($application, Response::HTTP_OK);
    }

    /**
     * Add application
     *
     * @OA\Tag(name="Applications")
     * @OA\Response(
     *     response=201,
     *     description="Returns created application",
     *     @OA\JsonContent(type="object",
     *      @OA\Property(type="object", property="data", ref=@Model(type=Application::class))
     *     )
     * )
     * @OA\Parameter(name="clientId", in="query", required=true, @OA\Schema(type="integer"))
     * @OA\Parameter(name="term", in="query", required=true, @OA\Schema(type="integer"))
     * @OA\Parameter(name="amount", in="query", required=true, @OA\Schema(type="number"))
     * @OA\Parameter(name="currency", in="query", required=true, @OA\Schema(type="string"))
     *
     * @param Request $request
     * @param ApplicationService $applicationService
     * @param SerializerInterface $serializer
     * @param ClientRepositoryInterface $clientRepository
     * @return Response
     * @throws \App\Exception\ValidationException
     */
    public function add(
        Request $request,
        ApplicationService $applicationService,
        SerializerInterface $serializer,
        ClientRepositoryInterface $clientRepository
    ): Response
    {
        $application = $serializer->deserialize($request->getContent(), Application::class, 'json');
        $applicationService->saveApplication($application);

        return $this->json($application, Response::HTTP_CREATED);
    }

    /**
     * Update application
     *
     * @OA\Tag(name="Applications")
     * @OA\Response(
     *     response=201,
     *     description="Returns updated application",
     *     @OA\JsonContent(type="object",
     *      @OA\Property(type="object", property="data", ref=@Model(type=Application::class))
     *     )
     * )
     * @OA\Parameter(name="clientId", in="query", required=true, @OA\Schema(type="integer"))
     * @OA\Parameter(name="term", in="query", required=true, @OA\Schema(type="integer"))
     * @OA\Parameter(name="amount", in="query", required=true, @OA\Schema(type="number"))
     * @OA\Parameter(name="currency", in="query", required=true, @OA\Schema(type="string"))
     *
     * @param Request $request
     * @param int $id
     * @param ApplicationRepositoryInterface $applicationRepository
     * @param ApplicationService $applicationService
     * @param SerializerInterface $serializer
     * @return Response
     * @throws \App\Exception\ValidationException
     */
    public function update(
        Request $request,
        int $id,
        ApplicationRepositoryInterface $applicationRepository,
        ApplicationService $applicationService,
        SerializerInterface $serializer
    ): Response
    {
        $application = $applicationRepository->find($id);
        if (!$application) {
            throw new NotFoundHttpException('Application not found.');
        }

        $serialized = new Application();
        $serializer->deserialize($request->getContent(), Application::class, 'json',  [
            AbstractNormalizer::OBJECT_TO_POPULATE => $serialized,
            'groups' => 'update'
        ]);

        $applicationService->updateApplication($application, $serialized);

        return $this->json($application, Response::HTTP_OK);
    }

    /**
     * Delete application
     *
     * @OA\Tag(name="Applications")
     * @OA\Response(
     *     response=200,
     *     description="Returns delete status",
     *     @OA\JsonContent(type="object",
     *      @OA\Property(type="object", property="data",
     *          @OA\Property(type="string", property="message", default="Application deleted."),
     *      )
     *     )
     * )
     *
     * @param int $id
     * @param ApplicationRepositoryInterface $applicationRepository
     * @param ApplicationService $applicationService
     * @return Response
     */
    public function delete(int $id, ApplicationRepositoryInterface $applicationRepository, ApplicationService $applicationService): Response
    {
        $application = $applicationRepository->find($id);
        if (!$application) {
            throw new NotFoundHttpException('Client not found.');
        }

        $applicationService->deleteApplication($application);

        return $this->json(['message' => 'Application deleted.'], Response::HTTP_OK);
    }
}
