<?php

declare(strict_types=1);

namespace App\Serializer\Normalizer;

use App\Entity\Application;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ApplicationNormalizer extends AbstractNormalizer implements NormalizerInterface, CacheableSupportsMethodInterface
{
    private NormalizerInterface $normalizer;

    public function __construct(ObjectNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    /**
     * @param mixed $object
     * @param string|null $format
     * @param array $context
     * @return array|ArrayObject|bool|float|int|string|void|null
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        return $this->normalizer->normalize($object, $format, [
            'ignored_attributes' => self::IGNORED_ATTRINUTES
        ]);
    }

    /**
     * @param mixed $data
     * @param string|null $format
     * @return bool|void
     */
    public function supportsNormalization($data, string $format = null)
    {
        return $data instanceof Application;
    }

    /**
     * @return bool
     */
    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}