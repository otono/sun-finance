<?php

declare(strict_types=1);

namespace App\Serializer\Normalizer;

class AbstractNormalizer
{
    const IGNORED_ATTRINUTES = [
        'lazyPropertiesNames',
        'lazyPropertiesDefaults',
        '__initializer__',
        '__cloner__',
        '__isInitialized__'
    ];
}