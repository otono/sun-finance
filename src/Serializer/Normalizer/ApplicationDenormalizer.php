<?php

declare(strict_types=1);

namespace App\Serializer\Normalizer;

use App\Entity\Application;
use App\Entity\Client;
use App\Repository\ClientRepositoryInterface;
use Symfony\Component\Serializer\Normalizer\CustomNormalizer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ApplicationDenormalizer extends CustomNormalizer implements DenormalizerInterface
{
    private ObjectNormalizer $normalizer;
    private ClientRepositoryInterface $clientRepository;

    /**
     * ApplicationDenormalizer constructor.
     * @param ObjectNormalizer $normalizer
     * @param ClientRepositoryInterface $clientRepository
     */
    public function __construct(ObjectNormalizer $normalizer, ClientRepositoryInterface $clientRepository)
    {
        $this->normalizer = $normalizer;
        $this->clientRepository = $clientRepository;
    }

    /**
     * @param mixed $data
     * @param string $type
     * @param string|null $format
     * @param array $context
     * @return Client|null
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalize($data, string $type, string $format = null, array $context = []): ?Application
    {
        $data['client'] = $data['clientId'];
        $object = $this->normalizer->denormalize($data, $type, $format, $context);
        $client = $this->clientRepository->find($data['client']);
        $object->setClient($client);

        return $object;
    }

    /**
     * @param mixed $data
     * @param string $type
     * @param string|null $format
     * @return bool
     */
    public function supportsDenormalization($data, string $type, string $format = null): bool
    {
        return $type == Application::class;
    }
}