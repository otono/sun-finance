<?php

namespace App\Entity;

use App\Repository\ClientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: ClientRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Client
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', length: 32)]
    #[Assert\Regex("/^[a-zA-Z]+$/", groups: ['update'])]
    #[Assert\Length(min: 2, max: 32, groups: ['update'])]
    #[Assert\NotBlank(groups: ['update'])]
    #[Groups('update')]
    private string $firstName;

    #[ORM\Column(type: 'string', length: 32)]
    #[Assert\Regex("/^[a-zA-Z]+$/", groups: ['update'])]
    #[Assert\Length(min: 2, max: 32, groups: ['update'])]
    #[Assert\NotBlank(groups: ['update'])]
    #[Groups('update')]
    private string $lastName;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Email(message: 'The email {{ value }} is not a valid email.', groups: ['update'])]
    #[Assert\Length(min: 6, max: 255, groups: ['update'])]
    #[Assert\NotBlank(groups: ['update'])]
    #[Groups('update')]
    private string $email;

    #[ORM\Column(type: 'string', length: 16)]
    #[Assert\Regex("/^\+?[1-9]\d{1,14}$/", groups: ['update'])]
    #[Assert\NotBlank(groups: ['update'])]
    #[Groups('update')]
    private string $phoneNumber;

    #[ORM\Column(type: 'datetime_immutable')]
    private \DateTimeImmutable $createdAt;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?\DateTimeImmutable $updatedAt;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: Application::class)]
    #[Ignore]
    private Collection $applications;

    public function __construct()
    {
        $this->applications = new ArrayCollection();
    }

    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    #[Ignore]
    public function setTimestamps()
    {
        $this->setUpdatedAt(new \DateTimeImmutable());
        if (!isset($this->createdAt)) {
            $this->setCreatedAt(new \DateTimeImmutable());
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    #[Ignore]
    public function getApplications(): Collection
    {
        return $this->applications;
    }

    public function addApplication(Application $application): self
    {
        if (!$this->applications->contains($application)) {
            $this->applications[] = $application;
            $application->setClient($this);
        }

        return $this;
    }

    public function removeApplication(Application $application): self
    {
        if ($this->applications->removeElement($application)) {
            // set the owning side to null (unless already changed)
            if ($application->getClient() === $this) {
                $application->setClient(null);
            }
        }

        return $this;
    }
}
