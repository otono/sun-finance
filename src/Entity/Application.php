<?php

namespace App\Entity;

use App\Repository\ApplicationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;

#[ORM\Entity(repositoryClass: ApplicationRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Application
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\ManyToOne(targetEntity: Client::class, inversedBy: 'applications')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Client $client;

    #[ORM\Column(type: 'smallint')]
    #[Assert\Range(min: 10, max: 30, groups: ['update'])]
    #[Assert\NotBlank(groups: ['update'])]
    #[Groups('update')]
    private int $term;

    #[ORM\Column(type: 'float')]
    #[Assert\Range(min: 100, max: 5000, groups: ['update'])]
    #[Assert\NotBlank(groups: ['update'])]
    #[Groups('update')]
    private float $amount;

    #[ORM\Column(type: 'string', length: 3)]
    #[Assert\Regex("/^[a-zA-Z]+$/", groups: ['update'])]
    #[Assert\Length(min: 3, max: 3, groups: ['update'])]
    #[Assert\NotBlank(groups: ['update'])]
    #[Groups('update')]
    private string $currency;

    #[ORM\Column(type: 'datetime_immutable')]
    private \DateTimeImmutable $createdAt;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?\DateTimeImmutable $updatedAt;

    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    #[Ignore]
    public function setTimestamps()
    {
        $this->setUpdatedAt(new \DateTimeImmutable());
        if (!isset($this->createdAt)) {
            $this->setCreatedAt(new \DateTimeImmutable());
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getTerm(): ?int
    {
        return $this->term;
    }

    public function setTerm(int $term): self
    {
        $this->term = $term;

        return $this;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
