<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Application;
use App\Exception\ValidationException;
use App\Repository\ApplicationRepositoryInterface;
use App\Repository\ClientRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class ApplicationService
{
    private ApplicationRepositoryInterface $applicationRepository;
    private ClientRepositoryInterface $clientRepository;
    private EntityManagerInterface $em;
    private ValidatorInterface $validator;

    /**
     * ApplicationService constructor.
     * @param ApplicationRepositoryInterface $applicationRepository
     * @param ClientRepositoryInterface $clientRepository
     * @param EntityManagerInterface $em
     * @param ValidatorInterface $validator
     */
    public function __construct(
        ApplicationRepositoryInterface $applicationRepository,
        ClientRepositoryInterface $clientRepository,
        EntityManagerInterface $em,
        ValidatorInterface $validator
    )
    {
        $this->applicationRepository = $applicationRepository;
        $this->clientRepository = $clientRepository;
        $this->em = $em;
        $this->validator = $validator;
    }

    /**
     * @param Application $application
     * @throws ValidationException
     */
    public function saveApplication(Application $application): void
    {
        // Validate
        $errors = $this->validator->validate($application, null, ['update']);
        if (count($errors)) {
            throw new ValidationException($errors);
        }

        $this->applicationRepository->save($application);

        if (!$application->getClient()) {
            throw new NotFoundHttpException('Client not found.');
        }

        $this->em->flush();
    }

    /**
     * @param Application $application
     * @param Application $data
     * @throws ValidationException
     */
    public function updateApplication(Application $application, Application $data): void
    {
        // Validate
        $errors = $this->validator->validate($data, null, ['update']);
        if (count($errors)) {
            throw new ValidationException($errors);
        }

        $application->setTerm($data->getTerm());
        $application->setAmount($data->getAmount());
        $application->setCurrency($data->getCurrency());

        $this->applicationRepository->save($application);

        $this->em->flush();
    }

    /**
     * @param Application $application
     */
    public function deleteApplication(Application $application): void
    {
        $this->applicationRepository->delete($application);
        $this->em->flush();
    }
}