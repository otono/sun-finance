<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Client;
use App\Exception\ClientExistsException;
use App\Exception\EntityConstraintException;
use App\Repository\ClientRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Exception\ValidationException;

final class ClientService
{
    private ClientRepositoryInterface $clientRepository;
    private EntityManagerInterface $em;
    private ValidatorInterface $validator;

    /**
     * ClientService constructor.
     * @param ClientRepositoryInterface $clientRepository
     * @param EntityManagerInterface $em
     * @param ValidatorInterface $validator
     */
    public function __construct(ClientRepositoryInterface $clientRepository, EntityManagerInterface $em, ValidatorInterface $validator)
    {
        $this->clientRepository = $clientRepository;
        $this->em = $em;
        $this->validator = $validator;
    }

    /**
     * @param Client $client
     * @throws ValidationException
     * @throws ClientExistsException
     */
    public function saveClient(Client $client): void
    {
        // Validate
        $errors = $this->validator->validate($client, null, ['update']);
        if (count($errors)) {
            throw new ValidationException($errors);
        }

        $this->clientRepository->save($client);
        $this->em->flush();
    }

    /**
     * @param Client $client
     * @param Client $data
     * @return void
     * @throws ValidationException
     */
    public function updateClient(Client $client, Client $data): void
    {
        // Validate
        $errors = $this->validator->validate($data, null, ['update']);
        if (count($errors)) {
            throw new ValidationException($errors);
        }

        $client->setFirstName($data->getFirstName());
        $client->setLastName($data->getLastName());
        $client->setEmail($data->getEmail());
        $client->setPhoneNumber($data->getPhoneNumber());

        $this->clientRepository->save($client);
        $this->em->flush();
    }

    /**
     * @param Client $client
     */
    public function deleteClient(Client $client): void
    {
        $this->clientRepository->delete($client);
        $this->em->flush();
    }
}