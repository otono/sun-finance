<?php

namespace App\Repository;

use App\Entity\Client;
use Doctrine\ORM\Query;

interface ClientRepositoryInterface
{
    public function getAllPaginatedQuery(): Query;
    public function save(Client $client): void;
    public function delete(Client $client): void;
}