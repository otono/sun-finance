<?php

namespace App\Repository;

use App\Entity\Application;
use Doctrine\ORM\Query;

interface ApplicationRepositoryInterface
{
    public function getAllPaginatedQuery(): Query;
    public function save(Application $application): void;
    public function delete(Application $application): void;
}