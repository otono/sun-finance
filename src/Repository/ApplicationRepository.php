<?php

namespace App\Repository;

use App\Entity\Application;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Application|null find($id, $lockMode = null, $lockVersion = null)
 * @method Application|null findOneBy(array $criteria, array $orderBy = null)
 * @method Application[]    findAll()
 * @method Application[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApplicationRepository extends ServiceEntityRepository implements ApplicationRepositoryInterface
{
    private EntityManagerInterface $em;

    /**
     * ApplicationRepository constructor.
     * @param ManagerRegistry $registry
     * @param EntityManagerInterface $em
     */
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $em)
    {
        parent::__construct($registry, Application::class);
        $this->em = $em;
    }

    /**
     * @return Query
     */
    public function getAllPaginatedQuery(): Query
    {
        return $this->createQueryBuilder('a')
            ->orderBy('a.createdAt', 'DESC')
            ->getQuery();
    }

    /**
     * @param Application $application
     */
    public function save(Application $application): void
    {
        $this->em->persist($application);
    }

    /**
     * @param Application $application
     */
    public function delete(Application $application): void
    {
        $this->em->remove($application);
    }
}
