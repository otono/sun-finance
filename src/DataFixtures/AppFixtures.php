<?php

namespace App\DataFixtures;

use App\Entity\Application;
use App\Entity\Client;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class AppFixtures extends Fixture
{
    private Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i <= 25; $i++) {
            $client = new Client();
            $client->setFirstName(substr($this->faker->firstName, 0, 32));
            $client->setLastName(substr($this->faker->lastName, 0, 32));
            $client->setEmail($this->faker->email);
            $client->setPhoneNumber('+'.substr(preg_replace('/[^0-9]/', '', $this->faker->phoneNumber), 0, 15));
            $manager->persist($client);

            $application = new Application();
            $application->setClient($client);
            $application->setTerm(rand(10,30));
            $application->setAmount(rand(100,5000));
            $application->setCurrency('EUR');
            $manager->persist($application);
        }

        $manager->flush();
    }
}
