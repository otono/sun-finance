<?php

declare(strict_types=1);

namespace App\EventListener\Handler;

use App\Exception\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ValidationExceptionHandler
{
    private ValidationException $exception;

    /**
     * ValidationExceptionHandler constructor.
     * @param ValidationException $exception
     */
    public function __construct(ValidationException $exception)
    {
        $this->exception = $exception;
    }

    /**
     * @return void
     */
    public function handle(): void
    {
        $response = new JsonResponse([
            'message' => $this->exception->getMessage(),
            'errors' => $this->exception->getErrors()
        ], Response::HTTP_BAD_REQUEST);

        $response->send();
    }
}