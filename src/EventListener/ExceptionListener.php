<?php

declare(strict_types=1);

namespace App\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Contracts\HttpClient\Exception\HttpExceptionInterface;

class ExceptionListener
{
    /**
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        // Check exception handler
        $handlerClass = __NAMESPACE__.'\\Handler\\'.substr(get_class($exception), strrpos(get_class($exception), '\\' )+1).'Handler';
        if (class_exists($handlerClass)) {
            $handler = new $handlerClass($exception);
            $handler->handle();
        }

        // Default
        $response = new JsonResponse([
            'message' => $exception->getMessage(),
//            'trace' => $exception->getTrace()
        ]);

        $event->setResponse($response);
    }
}