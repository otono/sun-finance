<?php

namespace App\Tests\Controller;

use App\Entity\Client;
use App\Repository\ClientRepositoryInterface;
use Doctrine\ORM\EntityManager;
use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ClientControllerTest extends WebTestCase
{
    public function testClientsList(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/api/clients');

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testAddClient(): void
    {
        $faker = Factory::create();
        $client = static::createClient();
        $client->request('POST', '/api/clients', [], [], [], json_encode([
            'firstName' => substr($faker->firstName, 0, 32),
            'lastName' => substr($faker->lastName, 0, 32),
            'email' => $faker->email,
            'phoneNumber' => '+'.substr(preg_replace('/[^0-9]/', '', $faker->phoneNumber), 0, 15),
        ]));

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);
    }

    public function testClientInfo(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/api/clients/1');

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testClientUpdate(): void
    {
        $faker = Factory::create();
        $client = static::createClient();
        $data = [
            'firstName' => substr($faker->firstName, 0, 32),
            'lastName' => substr($faker->lastName, 0, 32),
            'email' => $faker->email,
            'phoneNumber' => '+'.substr(preg_replace('/[^0-9]/', '', $faker->phoneNumber), 0, 15),
        ];
        $client->request('PUT', '/api/clients/1', [], [], [], json_encode($data));

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testClientDelete(): void
    {
        $client = static::createClient();
        $crawler = $client->request('DELETE', '/api/clients/1');

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }
}
