<?php

namespace App\Tests\Controller;

use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ApplicationControllerTest extends WebTestCase
{
    public function testApplicationsList(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/api/applications');

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testAddApplication(): void
    {
        $faker = Factory::create();
        $client = static::createClient();
        $client->request('POST', '/api/applications', [], [], [], json_encode([
            'clientId' => 1,
            'term' => rand(10,30),
            'amount' => rand(100,5000),
            'currency' => 'EUR',
        ]));

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);
    }

    public function testApplicationInfo(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/api/applications/1');

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testApplicationUpdate(): void
    {
        $faker = Factory::create();
        $client = static::createClient();
        $client->request('PUT', '/api/applications/1', [], [], [], json_encode([
            'clientId' => 1,
            'term' => rand(10,30),
            'amount' => rand(100,5000),
            'currency' => 'EUR',
        ]));

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testApplicationDelete(): void
    {
        $client = static::createClient();
        $crawler = $client->request('DELETE', '/api/applications/1');

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }
}
